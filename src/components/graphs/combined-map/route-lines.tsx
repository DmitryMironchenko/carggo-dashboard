import React, { ReactElement, useEffect, useState } from 'react'
import _ from 'lodash'
import { RouteLine } from './route-line'

interface ICoordinates {
  lat: string
  lng: string
}
export interface IRoute {
  from: ICoordinates
  to: ICoordinates
}

interface IProps {
  routes: IRoute[]
  projection: any
}

const RouteLines = ({ routes, projection }: IProps) => {
  const [lines, updateLines] = useState<ReactElement[]>([])

  useEffect(() => {
    const generatedLines = _.map(routes, route => {
      const lineId = `lane-${Number(route.from.lng) * _.random(1.2, 5.2)}`
      return <RouteLine key={lineId} lineId={lineId} route={route} projection={projection} />
    })
    updateLines(generatedLines)
  }, [routes, projection])

  return <>{lines}</>
}

export default RouteLines
