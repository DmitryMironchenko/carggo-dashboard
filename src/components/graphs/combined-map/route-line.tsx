import React, { useRef, useState, useEffect, ReactElement, useContext } from 'react'
import * as d3 from 'd3'
import _ from 'lodash'
import styled from 'styled-components'
import { MapContext } from './'

export interface Route {
  from: Coordinates
  to: Coordinates
}

const buildBezier = (x1: number, y1: number, x2: number, y2: number) => {
  const dx = (x1 + x2) / 2
  const dy = (y1 + y2) / 2 + Math.log(Math.abs(dx) || 1) * 10

  return { dx, dy }
}

const StyledSvg = styled.svg`
  position: absolute;
  width: 960px;
  height: 500px;
  top: -250px;
  left: -480px;
`

const StyledPath = styled.path`
  stroke: #122a5d;
  stroke-width: 2;
  fill: none;
  cursor: pointer;
`

const StyledPathOverlay = styled(StyledPath)`
  stroke: #fff;
  stroke-width: 5;
`

interface Props {
  route: Route
  lineId: number
  projection: any
}

export const RouteLine = ({ route, lineId, projection }: Props) => {
  const pathRef = useRef(null)
  const [showAnimation, toggleAnimation] = useState(false)
  const [points, updatePoints] = useState<ReactElement[]>([])
  const [x1y1x2y2, setCoords] = useState<{}>({ x1: null, x2: null, y1: null, y2: null })
  const [bezierCoordinates, setBezierCoordinates] = useState<any>({})

  const { value: mapContextData } = useContext(MapContext)

  function mouseOver(e: Event) {
    e.preventDefault()
    toggleAnimation(true)
  }

  function mouseLeave(e: Event) {
    e.preventDefault()
    toggleAnimation(false)
  }

  useEffect(() => {
    let duration = 3000
    let delay = 300

    if (pathRef.current) {
      const pathLength = pathRef.current.getTotalLength()
      const markerDistance = 40

      const markerCount = Math.floor(pathLength / markerDistance)
      duration = Math.log(pathLength / 3)
      delay = duration / markerCount
    }
    const [x1, y1] = projection([route.from.lng, route.from.lat])
    const [x2, y2] = projection([route.to.lng, route.to.lat])

    setCoords({
      x1,
      x2,
      y1,
      y2,
    })

    setBezierCoordinates(buildBezier(x1, y1, x2, y2))

    updatePoints(
      _.map(d3.range(0, duration, delay), (d, i) => (
        <circle r={2} strokeWidth={0} fill="#122a5d" key={`${lineId}${i}`}>
          <animateMotion dur={duration} begin={`${d}s`} repeatCount="indefinite">
            <mpath href={`#${lineId}`} />
          </animateMotion>
        </circle>
      ))
    )
  }, [route, projection])

  return (
    <g>
      {showAnimation && x1y1x2y2.x1 && (
        <StyledPathOverlay
          key={lineId}
          d={`M${x1y1x2y2.x1} ${x1y1x2y2.y1} Q ${bezierCoordinates.dx} ${bezierCoordinates.dy}
            ${x1y1x2y2.x2} ${x1y1x2y2.y2}`}
          onMouseOver={mouseOver}
          onMouseLeave={mouseLeave}
          strokeWidth={5}
        />
      )}
      {x1y1x2y2.x1 && (
        <StyledPath
          ref={pathRef}
          id={lineId}
          d={`M${x1y1x2y2.x1} ${x1y1x2y2.y1} Q ${bezierCoordinates.dx} ${bezierCoordinates.dy}
            ${x1y1x2y2.x2} ${x1y1x2y2.y2}`}
          onMouseOver={mouseOver}
          onMouseLeave={mouseLeave}
          // an option with arc instead of Bezier
          // d={`M${x1} ${y1}, A${357} ${362}, 0 0, 0 ${x2}, ${y2}`}
        />
      )}
      {showAnimation && (
        <g className="markers" style={{ pointerEvents: 'none' }}>
          {points}
        </g>
      )}
    </g>
  )
}
