import * as d3 from 'd3'
import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'

const StyledContainer = styled.div`
  height: 500px;
  width: 960px;
`

const google = window.google

interface IProps {
  children: ({ containerElement: HTMLDivElement }) => {}
}

const MapContainer: React.FC<{}> = ({ children }) => {
  const ref = useRef<HTMLDivElement>(null)
  const mapRef = useRef<google.maps.Map>(null)
  const [projection, setProjection] = useState<any>(null)
  const [containerElement, setContainerElement] = useState(null)

  useEffect(() => {
    let map = mapRef.current

    if (ref.current) {
      map = new google.maps.Map(ref.current, {
        zoom: 8,
        center: new google.maps.LatLng(37.76487, -122.41948),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      })

      const geocoder = new google.maps.Geocoder()
      geocoder.geocode({ address: 'USA' }, (results, status) => {
        if (!results.length || status === 'ZERO_RESULTS') {
          console.error('Geocode: No results found for', { address: 'USA' })
          return
        }

        // console.log('Geocode: Results found for', { address: 'USA' }, results)
        const ne = results[0].geometry.viewport.getNorthEast()
        const sw = results[0].geometry.viewport.getSouthWest()

        map.fitBounds(results[0].geometry.viewport)
      })

      // Overlay layer
      const overlay = new google.maps.OverlayView()

      overlay.onAdd = () => {
        const layer = d3
          .select(overlay.getPanes().overlayMouseTarget)
          .append('div')
          .attr('class', 'overlayContainer')

        setContainerElement(layer.node())

        // Prevent Event Bubbling
        // google.maps.OverlayView.preventMapHitsFrom(d3.select('div.stations').node())
        // Draw each marker as a separate SVG element.
        // We could use a single SVG, but what size would it have?
        // this.overlay.draw = this.onDraw
        overlay.draw = () => {
          const prj = args => {
            const [lng, lat] = args
            const proj = overlay.getProjection()
            const xy = proj.fromLatLngToDivPixel(new google.maps.LatLng(lat, lng))
            return [xy.x, xy.y]
          }
          setProjection(() => prj)
        }
      }
      overlay.setMap(map)
    }
  }, [])

  return (
    <StyledContainer ref={ref}>
      {containerElement && projection && children({ containerElement, projection })}
    </StyledContainer>
  )
}

export default MapContainer
