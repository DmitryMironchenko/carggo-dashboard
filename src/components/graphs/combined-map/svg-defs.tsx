import React from 'react'

const Defs = () => (
  <defs>
    <filter id="shadow" y="-40%" height="180%" width="130%" x="-10%">
      <feDropShadow dx="0" dy="1" stdDeviation="1" />
    </filter>
    <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
      <stop offset="0%" style={{ stopColor: 'rgb(255, 141, 134)' }} />
      <stop offset="100%" style={{ stopColor: 'rgb(255, 15, 0)' }} />
    </linearGradient>
  </defs>
)

export default Defs
