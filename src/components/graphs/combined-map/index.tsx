import * as d3 from 'd3'
import React, { useCallback, useEffect, useRef, createContext } from 'react'
import styled from 'styled-components'

import MapContainer from './MapContainer'
import OverlaySVGContainer from './overlay-svg-container'
import LocationPoints from './location-points'
import RouteLines from './route-lines'

import { IRoute } from './route-lines'

import Defs from './svg-defs'

const StyledSvg = styled.svg`
  overflow: visible;
`

export const MapContext = createContext<any>(null)

const google = window.google

interface IProps {
  cities?: Array<{}>
  routes: Route[]
}

interface IState {
  mapOverlayIsReady: boolean
  selectedItem?: {
    lat: number
    lng: number
  } | null
  mapContextData?: any
}

class CombinedMapWidget extends React.Component<IProps, IState> {
  static defaultProps = {
    data: [],
    routes: [],
  }

  private mapContextData: any = null

  private map?: google.maps.Map
  private ref = React.createRef<HTMLDivElement>()
  private testing = { value: this.mapContextData, updateValue: this.updateMapContextData }

  componentDidMount() {}

  render() {
    // const { mapOverlayIsReady, selectedItem } = this.state

    return (
      <MapContainer>
        {({ containerElement, projection }) => (
          <OverlaySVGContainer containerElement={containerElement}>
            <StyledSvg width="900" height="450">
              <Defs />
              <MapContext.Provider value={this.testing}>
                <RouteLines routes={this.props.routes} projection={projection} />
                <LocationPoints cities={this.props.cities} projection={projection} />
              </MapContext.Provider>
            </StyledSvg>
          </OverlaySVGContainer>
        )}
      </MapContainer>
    )
  }

  private updateMapContextData(value: any) {
    this.mapContextData = value
  }
}

export default CombinedMapWidget
