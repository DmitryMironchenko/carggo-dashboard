import React, { ReactElement, useEffect, useState, useContext } from 'react'
import styled from 'styled-components'
import _ from 'lodash'

import MapContext from '../combined-map'

const StyledSvg = styled.svg.attrs(({ left, top }) => ({
  top,
  left,
}))`
  width: 20px;
  height: 20px;
  position: absolute;
  top: ${props => props.top}px;
  left: ${props => props.left}px;
`

const StyledCircle = styled.circle`
  fill: url(#gradient);
  stroke: #fff;
  stroke-width: 2;
  cursor: pointer;
  &.active {
    stroke-width: 4;
    z-index: 999;
  }
`

interface Props {
  cities: any[]
  projection: any
}
const LocationPoints = props => {
  const { cities, projection } = props
  const [points, updatePoints] = useState<ReactElement[]>([])
  // const { value: mapContextData, updateValue: updateMapContextData } = useContext(MapContext)

  const mouseOver = (e: Event, x, y) => {
    e.preventDefault()
    e.target.classList.add('active')
    // updateMapContextData({
    //  ...mapContextData,
    //  selectedPoint: { x, y },
    // })
  }

  const mouseLeave = (e: Event) => {
    e.preventDefault()
    e.target.classList.remove('active')
    // updateMapContextData({ ...mapContextData, selectedPoint: [] })
  }

  useEffect(() => {
    updatePoints(
      _.map(cities, cityData => {
        const x = projection([cityData.lng, cityData.lat])[0]
        const y = projection([cityData.lng, cityData.lat])[1]

        return (
          <StyledCircle
            key={`${cityData.city}/${cityData.state}`}
            cx={x}
            cy={y}
            r={8}
            onMouseOver={e => mouseOver(e, x, y)}
            onMouseLeave={mouseLeave}
          />
        )
      })
    )
  }, [cities, projection])

  return <>{points}</>
}

export default LocationPoints
