import React from 'react'
import ReactDOM from 'react-dom'

interface IProps {
  containerElement: HTMLDivElement
  children: React.ReactElement[]
}

const OverlaySVGContainer: React.FC<IProps> = ({ containerElement, children }) =>
  ReactDOM.createPortal(children, containerElement)

export default OverlaySVGContainer
