import * as d3 from 'd3'
import React from 'react'
import _ from 'lodash'
import usStates from '../assets/us-states'
import { projection } from '..'

export const SvgUsaMap = () => {
  const pathGenerator = d3.geoPath().projection(projection)

  const svgStates = _.map(usStates.features, usState => (
    <path
      key={usState.properties.name}
      d={pathGenerator(usState)}
      stroke="#fff"
      strokeWidth={1}
      fillOpacity={0.2}
    />
  ))

  return <>{svgStates}</>
}
