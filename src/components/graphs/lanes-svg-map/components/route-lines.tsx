import React, { ReactElement, useEffect, useState } from 'react'
import _ from 'lodash'
import { RouteLine } from './route-line'

interface Coordinates {
  lat: string
  lng: string
}
export interface Route {
  from: Coordinates
  to: Coordinates
}

interface Props {
  routes: Route[]
}

export const RouteLines = ({ routes }: Props) => {
  const [lines, updateLines] = useState<ReactElement[]>([])

  useEffect(() => {
    const generatedLines = _.map(routes, route => {
      const lineId = `lane-${Number(route.from.lng) * _.random(1.2, 5.2)}`
      return <RouteLine key={lineId} lineId={lineId} route={route} />
    })
    updateLines(generatedLines)
  }, [routes])

  return <>{lines}</>
}
