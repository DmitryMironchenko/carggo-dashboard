import React, { ReactElement, useEffect, useState, useContext } from 'react'
import styled from 'styled-components'
import _ from 'lodash'
import { projection } from '..'
import { MapContext } from '..'

const StyledCircle = styled.circle`
  fill: url(#gradient);
  stroke: #fff;
  stroke-width: 2;
  cursor: pointer;
  &.active {
    stroke-width: 4;
    z-index: 999;
  }
`

interface Props {
  cities: any[]
}
export const LocationPoints = ({ cities }: Props) => {
  const [points, updatePoints] = useState<ReactElement[]>([])
  const { value: mapContextData, updateValue: updateMapContextData } = useContext(MapContext)

  const mouseOver = (e: Event, x: number, y: number) => {
    e.preventDefault()
    e.target.classList.add('active')
    updateMapContextData({
      ...mapContextData,
      selectedPoint: { x, y },
    })
  }

  const mouseLeave = (e: Event) => {
    e.preventDefault()
    e.target.classList.remove('active')
    updateMapContextData({ ...mapContextData, selectedPoint: [] })
  }

  useEffect(() => {
    updatePoints(
      _.map(cities, cityData => {
        const x = projection([cityData.lng, cityData.lat])[0]
        const y = projection([cityData.lng, cityData.lat])[1]

        return (
          <StyledCircle
            key={`${cityData.city}/${cityData.state}`}
            cx={x}
            cy={y}
            r={_.random(5, 12)}
            onMouseOver={e => mouseOver(e, x, y)}
            onMouseLeave={mouseLeave}
          />
        )
      })
    )
  }, [cities])

  return <>{points}</>
}
