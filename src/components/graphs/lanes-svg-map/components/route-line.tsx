import React, { useRef, useState, useEffect, useContext, ReactElement } from 'react'
import * as d3 from 'd3'
import _ from 'lodash'
import styled from 'styled-components'
import { MapContext, projection } from '../index'
import { Route } from './route-lines'

const buildBezier = (x1: number, y1: number, x2: number, y2: number) => {
  const dx = (x1 + x2) / 2
  const dy = (y1 + y2) / 2 + Math.log(Math.abs(dx) || 1) * 10

  return { dx, dy }
}

const StyledPath = styled.path`
  stroke: #122a5d;
  stroke-width: 2;
  fill: none;
  cursor: pointer;
`

const StyledPathOverlay = styled(StyledPath)`
  stroke: #fff;
  stroke-width: 5;
`

interface Props {
  route: Route
  lineId: number
}

export const RouteLine = ({ route, lineId }: Props) => {
  const pathRef = useRef(null)
  const [showAnimation, toggleAnimation] = useState(false)
  const [points, updatePoints] = useState<ReactElement[]>([])
  const { value: mapContextData } = useContext(MapContext)

  const [x1, y1] = projection([route.from.lng, route.from.lat])
  const [x2, y2] = projection([route.to.lng, route.to.lat])

  const bezierCoordinates = buildBezier(x1, y1, x2, y2)

  function mouseOver(e: Event) {
    e.preventDefault()
    toggleAnimation(true)
  }

  function mouseLeave(e: Event) {
    e.preventDefault()
    toggleAnimation(false)
  }

  useEffect(() => {
    if (pathRef === null) {
      return
    }
    const pathLength = pathRef.current.getTotalLength()
    const markerDistance = 40

    const markerCount = Math.floor(pathLength / markerDistance)
    const duration = Math.log(pathLength / 3)
    const delay = duration / markerCount

    updatePoints(
      _.map(d3.range(0, duration, delay), (d, i) => (
        <circle r={2} strokeWidth={0} fill="#122a5d" key={`${lineId}${i}`}>
          <animateMotion dur={duration} begin={`${d}s`} repeatCount="indefinite">
            <mpath href={`#${lineId}`} />
          </animateMotion>
        </circle>
      ))
    )
  }, [route])

  useEffect(() => {
    if (!mapContextData.selectedPoint) {
      return
    }

    const { x: selectedX, y: selectedY } = mapContextData.selectedPoint
    if ((x1 === selectedX && y1 === selectedY) || (x2 === selectedX && y2 === selectedY)) {
      toggleAnimation(true)
    } else {
      toggleAnimation(false)
    }
  }, [mapContextData.selectedPoint])

  return (
    <g>
      {showAnimation && (
        <StyledPathOverlay
          ref={pathRef}
          d={`M${x1} ${y1} Q ${bezierCoordinates.dx} ${bezierCoordinates.dy}
            ${x2} ${y2}`}
          onMouseOver={mouseOver}
          onMouseLeave={mouseLeave}
          strokeWidth={5}
        />
      )}
      <StyledPath
        ref={pathRef}
        id={lineId}
        d={`M${x1} ${y1} Q ${bezierCoordinates.dx} ${bezierCoordinates.dy}
            ${x2} ${y2}`}
        onMouseOver={mouseOver}
        onMouseLeave={mouseLeave}
        // an option with arc instead of Bezier
        // d={`M${x1} ${y1}, A${357} ${362}, 0 0, 0 ${x2}, ${y2}`}
      />
      {showAnimation && (
        <g className="markers" style={{ pointerEvents: 'none' }}>
          {points}
        </g>
      )}
    </g>
  )
}
