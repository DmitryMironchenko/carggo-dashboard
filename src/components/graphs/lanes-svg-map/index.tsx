import * as d3 from 'd3'
import React, { useState, createContext } from 'react'
import { SvgUsaMap } from './components/usa-map'
import { LocationPoints } from './components/location-points'
import { RouteLines, Route } from './components/route-lines'

export const MapContext = createContext<any>(null)

const MAP_WIDTH = 960
const MAP_HEIGHT = 500

export const projection = d3
  .geoAlbersUsa()
  .translate([MAP_WIDTH / 2, MAP_HEIGHT / 2])
  .scale(1000)

interface Props {
  cities: object[]
  routes: Route[]
}

const SvgMap = ({ cities, routes }: Props) => {
  const [mapContextData, updateMapContextData] = useState({})
  const testing = { value: mapContextData, updateValue: updateMapContextData }

  return (
    <div style={{ color: '#fafafa' }}>
      <svg width={MAP_WIDTH} height={MAP_HEIGHT} style={{ background: '#fff' }}>
        <defs>
          <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
            <stop style={{ stopColor: 'rgb(255, 141, 134)' }} offset="0%" />
            <stop style={{ stopColor: 'rgb(255, 15, 0)' }} offset="100%" />
          </linearGradient>
        </defs>
        <MapContext.Provider value={testing}>
          <SvgUsaMap />
          <RouteLines routes={routes} />
          <LocationPoints cities={cities} />
        </MapContext.Provider>
      </svg>
    </div>
  )
}

export default SvgMap
