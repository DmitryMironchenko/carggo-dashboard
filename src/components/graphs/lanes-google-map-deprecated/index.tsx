import * as d3 from 'd3'
import React, { useCallback, useEffect, useRef } from 'react'

import PopUpContainer from './popup-container'
import WarehouseInfoPopup from './warehouse-info-popup'

const google = window.google

interface IProps {
  data?: Array<{}>
}

interface IState {
  mapOverlayIsReady: boolean
  selectedItem?: {
    lat: number
    lng: number
  } | null
}

const styles = {
  foo: {
    height: 500,
    width: 960,
  },
}

class LanesMapWidget extends React.Component<IProps, IState> {
  static defaultProps = {
    data: [],
  }

  state: Readonly<IState> = {
    mapOverlayIsReady: false,
  }

  private map?: google.maps.Map
  private overlay?: google.maps.OverlayView

  // tslint:disable-next-line: member-access
  componentDidMount() {
    this.map = new google.maps.Map(d3.select('#map').node(), {
      zoom: 8,
      center: new google.maps.LatLng(37.76487, -122.41948),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    })

    const geocoder = new google.maps.Geocoder()
    geocoder.geocode({ address: 'USA' }, (results, status) => {
      if (!results.length || status === 'ZERO_RESULTS') {
        // console.log('Geocode: No results found for', { address: 'USA' })
        return
      }

      // console.log('Geocode: Results found for', { address: 'USA' }, results)
      const ne = results[0].geometry.viewport.getNorthEast()
      const sw = results[0].geometry.viewport.getSouthWest()

      this.map.fitBounds(results[0].geometry.viewport)
    })

    this.map.addListener('click', e => {
      console.log('[INFO] map clicked', e)
      this.setState({ selectedItem: null })
    })

    this.overlay = new google.maps.OverlayView()

    this.overlay.onAdd = () => {
      const layer = d3
        .select(this.overlay.getPanes().overlayMouseTarget)
        .append('div')
        .attr('class', 'stations')

      // Prevent Event Bubbling
      google.maps.OverlayView.preventMapHitsFrom(d3.select('div.stations').node())
      // Draw each marker as a separate SVG element.
      // We could use a single SVG, but what size would it have?
      this.overlay.draw = this.onDraw
    }
    this.overlay.setMap(this.map)

    // Popup Overlay
    const popupOverlay = new google.maps.OverlayView()
    popupOverlay.onAdd = () => {
      const layer = d3
        .select(this.overlay.getPanes().overlayMouseTarget)
        .append('div')
        .attr('id', 'modal-root')

      this.setState({ mapOverlayIsReady: true })
    }
    popupOverlay.setMap(this.map)
  }

  componentDidUpdate() {
    this.onDraw()
  }

  render() {
    const { mapOverlayIsReady, selectedItem } = this.state

    return (
      <>
        <svg width="1" height="1">
          <defs>
            <filter id="shadow" y="-40%" height="180%" width="130%" x="-10%">
              <feDropShadow dx="0" dy="1" stdDeviation="1" />
            </filter>
            <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" style={{ stopColor: 'rgb(255, 141, 134)' }} />
              <stop offset="100%" style={{ stopColor: 'rgb(255, 15, 0)' }} />
            </linearGradient>
          </defs>
        </svg>
        <div style={styles.foo} id="map">
          {mapOverlayIsReady && selectedItem && (
            <PopUpContainer>
              <WarehouseInfoPopup data={selectedItem} />
            </PopUpContainer>
          )}
        </div>
      </>
    )
  }

  private onDraw = () => {
    const projection = this.overlay.getProjection()
    const padding = 10

    if (!this.overlay.getPanes()) {
      return
    }

    const layer = d3.select(this.overlay.getPanes().overlayMouseTarget).select('div.stations')

    const markers = layer.selectAll('svg.marker').data(this.props.data)
    const marker = markers
      .each(transform) // update existing markers
      .enter()
      .append('svg')
      .each(transform)
      .attr('class', 'marker')
      .style('overflow', 'visible')
      .style('width', d => d.r * 2 + 7)
      .style('height', d => d.r * 2 + 4)

    markers.exit().remove()

    // Add a circle.
    marker
      .append('circle')
      .attr('r', d => d.r)
      .attr('cx', d => d.r + 4)
      .attr('cy', d => d.r + 2)
      .attr('class', 'node')
      .style('fill', 'url(#gradient)')
      .style('cursor', 'pointer')
      .style('stroke', '#fff')
      .style('stroke-width', 1)
      .style('opacity', 1)
      .style('filter', 'url("#shadow")')
      .on('mouseover', function(d) {
        this.style.stroke = '#fff'
        this.style.strokeWidth = 4
      })
      .on('mouseout', function(d) {
        this.style.stroke = '#fff'
        this.style.strokeWidth = 1
      })
      .on('click', d => {
        this.setState({ selectedItem: d })
      })

    // Add a label.
    // marker
    //   .append('text')
    //   .attr('x', d => d.r * 2 + 9)
    //   .attr('y', d => d.r + 3)
    //   .attr('dy', '.31em')
    //   .text(d => d.city)

    if (this.state.selectedItem) {
      const popup = d3
        .select(this.overlay.getPanes().overlayMouseTarget)
        .select('div#modal-root>div')

      const { lat, lng } = this.state.selectedItem
      const xy = projection.fromLatLngToDivPixel(new google.maps.LatLng(lat, lng))
      popup.style('left', xy.x + 5 + 'px').style('top', xy.y - 243 + 'px')
    }

    function transform(d) {
      d = new google.maps.LatLng(d.lat, d.lng)
      const dd = projection.fromLatLngToDivPixel(d)

      const r = Math.round(Math.random() * 15) + 1

      return d3
        .select(this)
        .datum(d => ({ ...d, r, ...dd }))
        .style('left', dd.x - padding + 'px')
        .style('top', dd.y - padding + 'px')
    }
  }
}

export default LanesMapWidget
