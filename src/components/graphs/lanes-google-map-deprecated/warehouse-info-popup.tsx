import React, { useRef, useEffect } from 'react'
import styled from 'styled-components'

const StyledPopup = styled.div`
  width: 250px;
  height: 190px;
  padding: 20px 40px 20px 20px;
  text-align: left;
  background-color: #fff;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.25);
  margin-bottom: 20px;
  font-size: 0.9rem;
  cursor: pointer;

  & > h4 {
    margin: 10px 0 0 0;
  }

  &::after {
    content: '';
    position: absolute;
    width: 0;
    height: 0;
    border-style: solid;
    left: 0;
    top: calc(100% - 20px);
    border-width: 0 0 20px 20px;
    border-color: transparent transparent transparent #fff;
  }
`

interface IProps {
  data?: any
}

const WarehouseInfoPopup: React.FC<IProps> = (props: IProps) => {
  const foo = useRef(null)

  useEffect(() => {
    window.google.maps.OverlayView.preventMapHitsFrom(foo.current)
  }, [foo])

  return (
    <StyledPopup ref={foo}>
      <h4>PHOENIX AZ</h4>
      <p>28 inbound lanes</p>
      <p>9 outbound lanes</p>
      <p>820 annual shipments</p>
    </StyledPopup>
  )
}

export default WarehouseInfoPopup
