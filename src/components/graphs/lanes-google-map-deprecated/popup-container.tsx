import React, { Component } from 'react'
import ReactDOM from 'react-dom'

interface IProps {
  children: React.ReactNode
}

class PopUpContainer extends Component<IProps, {}> {
  private el: HTMLElement | undefined
  private modalRoot: HTMLElement | undefined

  constructor(props: IProps) {
    super(props)
    this.el = document.createElement('div')
  }

  componentDidMount() {
    this.modalRoot = document.getElementById('modal-root')

    if (this.modalRoot) {
      this.modalRoot.appendChild(this.el)
    }
  }

  componentWillUnmount() {
    this.modalRoot && this.modalRoot.removeChild(this.el)
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.el)
  }
}

export default PopUpContainer
