import React from 'react';
import CCAuthenticator from './CCAuthenticator';

export default function WithCCAuthenticator(Component) {
  /*
  This one is intended to add some default properties to CCAuthenticator
   */
  return props => (
    <CCAuthenticator {...props}>
      <Component {...props} />
    </CCAuthenticator>
  );
}
