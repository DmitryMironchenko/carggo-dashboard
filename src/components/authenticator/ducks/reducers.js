import queryString from 'query-string';

import types from './types';
import createReducer from '../../../utils/reducer-creator';

export const reducerName = 'CCAuthenticator';

export const authStates = {
  signIn: 'signIn',
  signUp: 'signUp',
  confirmSignIn: 'confirmSignIn',
  confirmSignUp: 'confirmSignUp',
  forgotPassword: 'forgotPassword',
  verifyContact: 'verifyContact',
  signedIn: 'signedIn',
};

const initialState = {
  authState: authStates.signIn,
  isAuthPending: false,
  authError: null,
  userData: {},
  authToken: '',
};

const authenticatorReducer = createReducer(initialState)({
  'persist/REHYDRATE': (state, action) => {
    /* eslint-disable-next-line no-restricted-globals */
    const { authToken } = queryString.parse(location.search);

    const newState = {
      ...state,
      ...(action.payload || {}).CCAuthenticator,
      ...(authToken
        ? {
            authState: authStates.signedIn,
            authToken,
            userData: { token: authToken },
          }
        : null),
    };

    return newState;
  },
  [types.SIGN_IN]: () => ({
    ...initialState,
    isAuthPending: true,
    authError: null,
  }),
  [types.SIGN_IN_PENDING]: () => ({
    authState: authStates.signIn,
  }),
  [types.SIGN_OUT]: () => ({
    ...initialState,
    authState: authStates.signIn,
    authToken: '',
  }),
  [types.SIGN_IN_SUCCESSFUL]: (state, action) => ({
    ...state,
    authToken: action.authToken,
    authState: authStates.signedIn,
    isAuthPending: false,
    authError: null,
    userData: action.userData,
  }),
  [types.SIGN_IN_FAILED]: (state, action) => ({
    ...state,
    isAuthPending: false,
    authError: action.errorMessage,
  }),
});

export default authenticatorReducer;
