import { authStates, reducerName } from './reducers';

const selectToken = state => state[reducerName].authToken;
const selectSignInData = state => state[reducerName];
const isUserSignedIn = state =>
  state[reducerName].authState === authStates.signedIn;
const getAuthState = state => state[reducerName].authState;
const getAuthError = state => state[reducerName].authError;

export default {
  selectToken,
  selectSignInData,
  isUserSignedIn,
  getAuthState,
  getAuthError,
};
