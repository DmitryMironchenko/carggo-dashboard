import { push } from 'connected-react-router';

import {
  signInPending,
  signInFailed,
  signInSuccessful,
  signOut as signOutAction,
} from './actions';
import { DMPParamsBuilder } from '../../../utils/requests-handling/DMPParamsBuilder';
import ErrorHandler from '../../../utils/requests-handling/error-handler';
import { DMPPost } from '../../../utils/requests-handling/DMPRest';

export const signOut = () => async dispatch => {
  await dispatch(signOutAction());
  await dispatch(push('/signin'));
};

export const signIn = data => async dispatch => {
  dispatch(signInPending());

  const paramBuilder = new DMPParamsBuilder('auth.users')
    .withPostfix('login/')
    .withData(data)
    .withErrorHandler(
      new ErrorHandler(({ e }) => {
        dispatch(signInFailed(e));
      }),
    );

  const rawData = await DMPPost(paramBuilder).run();

  if (rawData) {
    await dispatch(signInSuccessful(rawData));
    await dispatch(push('/'));
  }
};

export default {
  signIn,
  signOut,
};
