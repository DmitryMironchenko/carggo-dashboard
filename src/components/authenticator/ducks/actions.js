import types from './types';

export const signOut = () => ({
  type: types.SIGN_OUT,
});

export const signInPending = () => ({
  type: types.SIGN_IN_PENDING,
});

export const signInSuccessful = data => ({
  type: types.SIGN_IN_SUCCESSFUL,
  authToken: data.token,
  userData: data,
});

export const signInFailed = errorMessage => ({
  type: types.SIGN_IN_FAILED,
  errorMessage,
});

export default {
  signOut,
  signInPending,
  signInSuccessful,
  signInFailed,
};
