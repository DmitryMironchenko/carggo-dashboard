import reducer from './reducers';

export { default as authenticatorSelectors } from './selectors';
export { default as authenticatorOperations } from './operations';
export { default as authenticatorTypes } from './types';

export default reducer;
