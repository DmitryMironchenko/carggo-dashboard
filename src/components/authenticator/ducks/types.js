const SIGN_IN = 'authenticator/SIGN_IN';
const SIGN_OUT = 'authenticator/SIGN_OUT';
const SIGN_IN_PENDING = 'authenticator/SIGN_IN_PENDING';
const SIGN_IN_FAILED = 'authenticator/SIGN_IN_FAILED';
const SIGN_IN_SUCCESSFUL = 'authenticator/SIGN_IN_SUCCESSFUL';

export default {
  SIGN_IN,
  SIGN_OUT,
  SIGN_IN_FAILED,
  SIGN_IN_PENDING,
  SIGN_IN_SUCCESSFUL,
};
