import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import SignIn from './SignIn';
import { authenticatorSelectors } from './ducks/index';

const AUTH_STATES = {
  SIGN_IN: 'signIn',
  SIGN_UP: 'signUp',
  CONFIRM_SIGN_IN: 'confirmSignIn',
  CONFIRM_SIGN_UP: 'confirmSignUp',
  FORGOT_PASSWORD: 'forgotPassword',
  VERIFY_CONTACT: 'verifyContact',
  SIGNED_IN: 'signedIn',
};

class CCAuthenticator extends Component {
  state = {
    authState: this.props.authState || AUTH_STATES.SIGN_IN,
  };

  foo = () => (
    <>
      {React.Children.map(this.props.children, (child, index) =>
        React.cloneElement(child, {
          index,
          authState: this.state.authState,
        }),
      )}
    </>
  );

  render() {
    return (
      <Switch>
        <Route exact path="/signin" component={SignIn} />
        {this.props.isUserSignedIn && <Route path="/" component={this.foo} />}
        <Redirect push to="/signin" />
      </Switch>
    );
  }
}

export default connect(
  state => ({
    authState: authenticatorSelectors.getAuthState(state),
    isUserSignedIn: authenticatorSelectors.isUserSignedIn(state),
  }),
  dispatch => ({
    push: url => dispatch(push(url)),
  }),
)(CCAuthenticator);
