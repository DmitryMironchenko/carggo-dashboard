import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn } from '../ducks/operations';
import {
  StyledSignInLayout,
  StyledSignInWrapper,
  StyledSignInContainer,
  StyledSignInTab,
  StyledInputContainer,
  StyledSignInInput,
  StyledSignInButton,
  StyledHeader,
  StyledFooterWrapper,
  StyledErrorTooltip,
} from './SignInStyledComponents';
import { Footer } from '../../common-components/footer/index';
import { authenticatorSelectors } from '../ducks/index';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      token: '',
    };
  }

  handleUsernameChange = e => {
    this.setState({ username: e.target.value });
  };

  handlePasswordChange = e => {
    this.setState({ password: e.target.value });
  };

  handleTokenChane = e => {
    this.setState({ token: e.target.value });
  };

  render() {
    return (
      <StyledSignInLayout>
        <StyledSignInWrapper>
          <StyledSignInContainer>
            <StyledHeader>{process.env.REACT_APP_PRODUCT_NAME}</StyledHeader>
            <StyledErrorTooltip signInFailed={this.props.authError}>
              <span>Incorrect credentials provided. Please, try again.</span>
            </StyledErrorTooltip>
            <StyledSignInTab>
              <StyledInputContainer>
                <StyledSignInInput
                  placeholder="Username"
                  id="login"
                  label="Login"
                  margin="normal"
                  autoComplete="login"
                  required
                  value={this.state.username}
                  onChange={this.handleUsernameChange}
                  signInFailed={this.props.authError}
                />
                <StyledSignInInput
                  placeholder="Password"
                  id="password"
                  label="Password"
                  autoComplete="password"
                  type="password"
                  margin="normal"
                  required
                  value={this.state.password}
                  onChange={this.handlePasswordChange}
                  signInFailed={this.props.authError}
                />
              </StyledInputContainer>

              <StyledSignInButton
                disabled={
                  !this.state.token &&
                  !(this.state.username && this.state.password)
                }
                onClick={() => {
                  const data = {
                    username: this.state.username,
                    password: this.state.password,
                  };
                  this.props.onLoginClick(data);
                }}
                variant="raised"
                color="primary"
              >
                Sign In
              </StyledSignInButton>
            </StyledSignInTab>
          </StyledSignInContainer>
        </StyledSignInWrapper>
        <StyledFooterWrapper>
          <Footer />
        </StyledFooterWrapper>
      </StyledSignInLayout>
    );
  }
}

const mapStateToProps = state => ({
  authError: authenticatorSelectors.getAuthError(state),
});

const mapDispatchToProps = {
  onLoginClick: signIn,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignIn);
