import styled from 'styled-components';

export const StyledSignInLayout = styled.div`
  display: flex;
  flex-direction: column;
  grid-template-rows: auto 40px;
  min-height: 100vh;
`;

export const StyledSignInWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background: #fff;
`;

export const StyledSignInContainer = styled.div``;

export const StyledFooterWrapper = styled.div`
  width: 100%;
`;

export const StyledSignInTabsContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const StyledSignInTabButton = styled.button`
  cursor: pointer;
  height: 50px;
  width: 200px;
  padding: 0;
  margin-bottom: 41.5px;
  font-size: 14px;
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: uppercase;
  text-align: center;
  outline: none;
  transition: all 0.3s;
  border: none;
  color: ${props => (props.selected ? '#0098a0' : '#7f8181')};
  border-bottom: ${props =>
    props.selected ? '4px solid #46cfa4' : '4px solid #fafafa'};
`;

export const StyledSignInTab = styled.div`
  display: flex;
  flex-direction: column;
`;

export const StyledInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100px;
`;

export const StyledSignInInput = styled.input`
  padding: 8px;
  margin: 0 0 13px;
  border: ${props =>
    props.signInFailed ? '1px solid #de2f2a' : '1px solid #cccecf'};
  background: #ffffff;
  font-size: 14px;
  font-weight: 400;
  outline: none;
  color: #7f8181;
  font-family: 'Open Sans', sans-serif;
  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus,
  &:-webkit-autofill:active {
    background-color: #fff !important;
    color: #7f8181 !important;
  }
  &::-webkit-input-placeholder {
    color: #c4c5c5;
  }
  &:focus {
    color: #7f8181;
  }
`;

export const StyledSignInButton = styled(StyledSignInTabButton)`
  height: 35px;
  width: 100%;
  text-transform: none;
  margin: 30px 0;
  background: #f4f5f7;
  font-family: 'Open Sans', sans-serif;
  font-size: 13px;
  font-weight: 600;
  color: #435373;
  cursor: ${props => (props.disabled ? 'auto' : 'pointer')};
  border: none;
  border-radius: 3px;
  &:hover {
    background: ${props => (props.disabled ? '#f4f5f7' : '#e0e1e3')};
  }
`;

export const StyledHeader = styled.h1`
  margin-bottom: 72px;
  font-family: 'Open Sans', sans-serif;
  font-weight: 400;
  text-align: center;
`;

export const StyledErrorTooltip = styled.div`
  display: ${props => (props.signInFailed ? 'block' : 'none')};
  text-align: center;
  margin: -50px 0 20px 0;

  span {
    color: #de2f2a;
    font-size: 16px;
    font-weight: 600;

    font-family: 'Open Sans', sans-serif;
  }
`;
