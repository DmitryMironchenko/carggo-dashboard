import _ from 'lodash'
import React from 'react'
import { storiesOf, addParameters } from '@storybook/react'
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs'

import cities from './stubs/cities-100'
import Lanes from '../../components/graphs/combined-map'

const generatePairsOfPoints = (points: any[], pairsNumber: number) => {
  if (points.length < 2) {
    return []
  }
  const pointPairs = []
  for (let i = 0; i < pairsNumber; i++) {
    const linesPair = _.sampleSize(points, 2)
    pointPairs.push({
      from: { lat: linesPair[0].lat, lng: linesPair[0].lng },
      to: { lat: linesPair[1].lat, lng: linesPair[1].lng },
    })
  }

  return pointPairs
}

export const stories = storiesOf('Components|Map Component/Google Maps Lanes Graph', module)
stories.addDecorator(withKnobs)
stories
  .add('with no data', () => <Lanes />)
  .add('with data', () => {
    const label = 'Cities'
    const defaultValue = 10
    const options = {
      range: true,
      min: 0,
      max: cities.length,
      step: 1,
    }
    const groupId = 'GROUP-ID1'

    const citiesLabel = 'Cities'
    const citiesDefaultValue = Math.floor(cities.length / 2)
    const citiesOptions = {
      range: true,
      min: 0,
      max: cities.length,
      step: 1,
    }

    const numberOfCities = number(citiesLabel, citiesDefaultValue, citiesOptions, groupId)
    const randomCities = _.slice(cities, 0, numberOfCities)

    const routesLabel = 'Routes'
    const routesDefaultValue = 100
    const routesOptions = {
      range: true,
      min: 0,
      max: 200,
      step: 1,
    }
    const numberOfRoutes = number(routesLabel, routesDefaultValue, routesOptions, groupId)

    return (
      <Lanes cities={randomCities} routes={generatePairsOfPoints(randomCities, numberOfRoutes)} />
    )
  })
