import _ from 'lodash'
import React from 'react'
import { storiesOf, addParameters } from '@storybook/react'
import { themes } from '@storybook/theming'
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs'

import cities from './stubs/cities-100'

import SvgMap from '../../components/graphs/lanes-svg-map'

const generatePairsOfPoints = (points: any[], pairsNumber: number) => {
  if (points.length < 2) {
    return []
  }
  const pointPairs = []
  for (let i = 0; i < pairsNumber; i++) {
    const linesPair = _.sampleSize(points, 2)
    pointPairs.push({
      from: { lat: linesPair[0].lat, lng: linesPair[0].lng },
      to: { lat: linesPair[1].lat, lng: linesPair[1].lng },
    })
  }

  return pointPairs
}

addParameters({
  options: {
    theme: themes.dark,
  },
})

export const svgMapStories = storiesOf('Components|Map Component/Svg Map Lanes Graph', module)
svgMapStories.addDecorator(withKnobs)
svgMapStories.add('with data', () => {
  const groupId = 'Main Params'

  const citiesLabel = 'Cities'
  const citiesDefaultValue = Math.floor(cities.length / 2)
  const citiesOptions = {
    range: true,
    min: 0,
    max: cities.length,
    step: 1,
  }

  const numberOfCities = number(citiesLabel, citiesDefaultValue, citiesOptions, groupId)
  const randomCities = _.slice(cities, 0, numberOfCities)

  const routesLabel = 'Routes'
  const routesDefaultValue = 100
  const routesOptions = {
    range: true,
    min: 0,
    max: 200,
    step: 1,
  }
  const numberOfRoutes = number(routesLabel, routesDefaultValue, routesOptions, groupId)

  return (
    <SvgMap cities={randomCities} routes={generatePairsOfPoints(randomCities, numberOfRoutes)} />
  )
})
