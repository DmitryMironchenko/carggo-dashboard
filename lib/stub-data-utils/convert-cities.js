const csv = require('csv-parser')
const fs = require('fs-extra')
const _ = require('lodash')

const results = []

fs.createReadStream('./uscitiesv1.5.csv')
  .pipe(csv())
  .on('data', data => {
    const { city, state_id: state, lat, lng } = data
    results.push({
      city,
      state,
      lat,
      lng,
    })
  })
  .on('end', () => {
    fs.writeJsonSync(
      './cities.json',
      _.sampleSize(
        _.filter(results, r => r.state !== 'PR' && r.state !== 'HI' && r.state !== 'AK'), 100
      ),
    )
  })
